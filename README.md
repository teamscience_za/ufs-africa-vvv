# VVV - UFS Africa
Setup for local development

## Prerequisites
* Latest version of [Vagrant](https://www.vagrantup.com/)
* Latest version of [VirtualBox](https://www.virtualbox.org/)
* Working [VVV3](https://varyingvagrantvagrants.org/) Configuration

## Development Environment Setup
Config can be added to `config/config.yml` in the main VVV folder like this

```yaml

<site-name>:
    skip_provisioning: false
    description: "<description-here>"
    repo: git@bitbucket.org:teamscience_za/vvv-<site-name>.git
    hosts:
      - <site-name>.test
    custom:
      delete_default_plugins: true
      live_url: <live-url> # Redirect any uploads not found locally to this domain
      wpconfig_constants:
        WP_DEBUG: true
        WP_DEBUG_LOG: true
        WP_DISABLE_FATAL_ERROR_HANDLER: true # To disable in WP 5.2 the FER mode
        
```

Save the file and run `vagrant up --provision` to update VVV with the new site.


